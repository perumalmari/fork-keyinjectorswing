package com.mcpayment.app.impl;

public class LoadPropertyImpl {
	private String accessCode;

	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}

	public String getAccessCode() {
		return accessCode;
	}
}