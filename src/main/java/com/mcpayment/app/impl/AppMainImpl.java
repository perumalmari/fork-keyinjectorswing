package com.mcpayment.app.impl;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.mcpayment.app.AppMain;
import com.mcpayment.app.RestClient;
import com.mcpayment.app.UserScreen;
import com.mcpayment.utility.AppException;
import com.mcpayment.utility.IntegrityChecker;

@Component("appmain") 
public class AppMainImpl implements AppMain {
	
   private IntegrityChecker _checker; 
   private RestClient _restclient; 
   
   @Autowired
   public void setIntegerityChecker( IntegrityChecker checker ){
	   _checker = checker;
   }
   
   @Autowired
   public void setRestClient( RestClient client ){ 
	   _restclient = client;
   }
   
   public void RegisterFutureKey(String serialno, Boolean isNew,  
		   						 String futureKey, String accessKey){
        try{
        	List<String> hashtargets = new ArrayList<String>();
        	hashtargets.add(serialno); 
        	hashtargets.add(isNew ? "Y" : "N");
        	hashtargets.add(futureKey);
        	hashtargets.add(accessKey);
        	String encryptedHash = _checker.getMD5Hash(hashtargets);
            StringBuffer sendData = new StringBuffer();
           // sendData.append(XML_START);
            sendData.append("<dataset>");
            sendData.append(String.format("<serialno>%s</serialno>", serialno ));
            sendData.append(String.format("<isnew>%s</isnew>",isNew ? "Y" : "N"));
            sendData.append(String.format("<futurekey>%s</futurekey>",futureKey));
            sendData.append(String.format("<hash>%s</hash>",encryptedHash));
            sendData.append("</dataset>");
            String response = _restclient.callWebService(sendData.toString(), "registerfuturekey");
            
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(response));
            Document doc = db.parse(is);
            
            UserScreen us = new UserScreen();
            us.ResponseMessage(doc);
            
        }catch(Exception ex){
            throw new AppException(ex,"AppMainImpl:RegisterFutureKey","01","registration failed");   
        }        
    }   
   
 
}
