package com.mcpayment.app.impl;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import com.mcpayment.app.RestClient;
import com.mcpayment.utility.AppException;
import com.mcpayment.utility.SSLUtility;
/**
 *
 * @author mcpadmin
 */
public class RestClientImpl implements RestClient{
	
	private String theURLStr;	
	public void setUrlStr( String u ){
		theURLStr = u;  
	}
	
	private String thePortStr;
	public void setPortStr( String portStr ){
		thePortStr = portStr;  
	}	
	
	private String theApplicationName;
	public void setApplicationName( String name ){
		theApplicationName = name;  
	}		
	
	private String isSSL; // Y or N 
	public void setIsSSL( String YorN ){
		isSSL = YorN;  
	}
	
	
    public String callWebService(String sendXMLData, String MethodName ){	
    	
    	HttpURLConnection conn = null;
    	URL url = null;
    	OutputStreamWriter osw = null;
    	InputStream    is = null;
    	BufferedReader reader = null;
    	
    	String Host = String.format("http%s://%s:%s/%s/%s",
    			                    isSSL.equals("Y") ? "s" : "",
    			                    theURLStr,
    			                    thePortStr,
    			                    theApplicationName,
    			                    MethodName); 
    	
    	try {  
    		if(isSSL.equals("Y")) {  
    		  SSLUtility.disableCertificateValidation(); 
    		}   		
    		
			url = new URL(Host);
	 	    conn = (HttpURLConnection)url.openConnection();
	 	    conn.setDoOutput(true);
	 	    conn.setDoInput(true);
	 	    conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-type","text/xml");
			HttpURLConnection.setFollowRedirects(false); 
			conn.setInstanceFollowRedirects(false); 
						
			osw = new OutputStreamWriter(conn.getOutputStream());
			if (sendXMLData.length() > 0) osw.write(sendXMLData);
	        osw.flush();
	        osw.close();
	        osw = null;
	 
	        is = conn.getInputStream();
	        reader = new BufferedReader(new InputStreamReader(is));
	        String s;
	        String response = "";
	        while ((s = reader.readLine()) != null) response += s;
	        
	        reader.close();
	        reader = null;
	        is.close();
	        is = null;
	 
	        conn.disconnect();
	        conn = null;
	        
	        return response;
    	}catch (Exception e) {
		    try {
                if (osw != null) {
                    osw.close();
                    osw = null;
                }
                if (reader != null) {
                    reader.close();
                    reader = null;
                }
                if (is != null) {
                    is.close();
                    is = null;
                }
                if (conn != null) {
                    conn.disconnect();
                    conn = null;
                }
            } catch (Exception e2) {}
		    throw new AppException(e, "RestClientImpl:callWebService", "00", "failed in the communication with server" );
		}
    }    
}
