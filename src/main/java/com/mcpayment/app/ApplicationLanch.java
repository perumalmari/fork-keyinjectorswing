package com.mcpayment.app;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/*import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.mcpayment.app.impl.LoadPropertyImpl;*/

public class ApplicationLanch extends Thread {
	Graphics2D g2;
	JButton connect;
	JFrame frame;

	public void paint(Graphics g) {
		g2 = (Graphics2D) g;
	}

	public ApplicationLanch() {
		/* Testing the Loading data from property file.
		ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:app-context-annotation.xml");
		LoadPropertyImpl loadProp = (LoadPropertyImpl) ctx.getBean("propertyLoader");
		loadProp.readAccessCode();*/
		
		frame = new JFrame("DUKPT APPLICATION");
		
		JPanel mainpanel = new JPanel();
		mainpanel.setLayout(new BoxLayout(mainpanel, BoxLayout.Y_AXIS));
		JPanel panel1 = new JPanel();
		mainpanel.add(panel1);
		
		JLabel label = new JLabel("Welcome To Dukpt Application.");
		label.setSize(15, 15);
		panel1.add(label);
		
		JPanel panel2 = new JPanel();
		connect = new JButton("Click to Lanch Appln");
		connect.setSize(50, 100);
		connect.addActionListener(new ButtonListener());
		panel2.add(connect);
		mainpanel.add(panel2);
		
		frame.getContentPane().add(mainpanel);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 300);
	}

	public class ButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
		 
			if (e.getSource() == connect) {
		     	 new UserScreen();
		     	 frame.setVisible(false);
			}
		}
	}

	public static void main(String[] args) {
		new ApplicationLanch();
	}
}