package com.mcpayment.app;


import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.junit.runner.RunWith;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.mcpayment.app.impl.AppMainImpl;
import com.mcpayment.app.impl.LoadPropertyImpl;
import com.mcpayment.utility.KeyInjector;
import com.mcpayment.utility.RandomHexaNoGeneration;

@RunWith (SpringJUnit4ClassRunner.class)
public class UserScreen extends Thread {
	Graphics2D g2;
	JButton connect;
	JTextField jSerialNo;
	JTextField responseDescription;
	JFrame frame;
	JComboBox <String> list;
	

	public void paint(Graphics g) { 
		g2 = (Graphics2D) g;
	}

	public UserScreen() {
		frame = new JFrame("DUKPT APPLICATION");
		JLabel label;
		JPanel mainpanel = new JPanel();
		mainpanel.setLayout(new GridLayout(0,1));

		label = new JLabel("Please enter serial no & Future key!!");
		mainpanel.add(label);
		
		JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayout(2,2));
		mainpanel.add(panel1);
		
		label = new JLabel("Serial No:");
		panel1.add(label);
		
		jSerialNo =  new JTextField(10);
		panel1.add(jSerialNo);
		
		label = new JLabel("COM");
		panel1.add(label);
		String[] listComNames = { "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9", "COM10" };
		list = new JComboBox <String>(listComNames);
		panel1.add(list);
		
		JPanel panel3 = new JPanel();
		panel3.setLayout(new GridLayout(1,2));
		mainpanel.add(panel3);
		
		connect = new JButton("Inject Key!!!");
		connect.addActionListener(new ButtonListener());
		connect.setPreferredSize(new Dimension(15, 20));
		panel3.add(connect);
		
		label = new JLabel("");
		panel3.add(label);
		
		label = new JLabel("Result!!!!");
		mainpanel.add(label);
		
		JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayout(1,2));
		mainpanel.add(panel2);
		
		label = new JLabel("Response Description:");
		panel2.add(label);
		
		responseDescription  = new JTextField(10);
		responseDescription.setEditable(false);
		panel2.add(responseDescription);
		
		frame.getContentPane().add(mainpanel);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(450, 300);
	}

	public class ButtonListener implements ActionListener {
		private GenericXmlApplicationContext ctx;
		private RandomHexaNoGeneration rhng;
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == connect) {
				rhng =  new RandomHexaNoGeneration();
				ctx = new GenericXmlApplicationContext();
		    	ctx.load("classpath:app-context-annotation.xml" );
		    	ctx.refresh(); 
		        
			    AppMain appMain = ctx.getBean("appmain", AppMainImpl.class );
			    LoadPropertyImpl loadProp = (LoadPropertyImpl) ctx.getBean("propertyLoader");
			    appMain.RegisterFutureKey(jSerialNo.getText(), true, rhng.get16ByteRandomHexaDecimalNumber() , loadProp.getAccessCode());
			    
			    // Connecting to dango devices...... for key injection.
			    int selectCom = Integer.parseInt(list.getSelectedItem().toString().substring(list.getSelectedItem().toString().length() -1, list.getSelectedItem().toString().length()));
			    
			    
			    
			    KeyInjector ki = new KeyInjector();
			    String returnStr = ki.InjectKey(jSerialNo.getText(), 
			    			rhng.get16ByteRandomHexaDecimalNumber(),
			    			selectCom, true);
			    
			    System.out.println("Return String"+returnStr);
			    frame.setVisible(false);
			}
		}
	}

	public void ResponseMessage (Document doc) {
		doc.getDocumentElement().normalize();
		NodeList nList = doc.getElementsByTagName("dataset");
		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				if (doc.getElementsByTagName("description").getLength() == 1){
					responseDescription.setText("Successfull inserted.");
				} else {
					responseDescription.setText(eElement.getElementsByTagName("returncode").item(0).getTextContent());
				}
			}
		}
	}

	public static void main(String[] args) {
		new UserScreen();
	}
}