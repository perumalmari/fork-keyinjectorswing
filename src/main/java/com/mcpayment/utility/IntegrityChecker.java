package com.mcpayment.utility;

import java.util.List;

public interface IntegrityChecker {
	String getMD5Hash(List<String> AllParametersExceptHash ) throws Exception;
}
