package com.mcpayment.utility;


public class AppException extends RuntimeException{ 
	private static final long serialVersionUID = 1L;
	// public static final String sysErrMsg = "System is not available. Please try again later.";
    private String code;
    private String loc;
    private String dispMsg;
    
    public AppException(String loc, 
                        String code, 
                        String dispMsg){
        super(dispMsg);
        this.loc = loc;
        this.code = code;        
        this.dispMsg = dispMsg;
    }

    public AppException(Throwable e, 
                        String loc, 
                        String code, 
                        String dispMsg){
        super(dispMsg, e);        
        this.loc = loc;
        this.code = code;
        this.dispMsg = dispMsg;   
    }   
    
    
    public String getLoc(){
        return loc;
    }

    public String getCode(){
        if(code.equals("")) return "XXXX";
        return code;
    }

    public String getDispMsg(){
        return dispMsg == null ? "Error" : dispMsg;
    }
}