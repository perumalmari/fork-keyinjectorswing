package com.mcpayment.utility;

import java.util.Random;

public class RandomHexaNoGeneration {
	public String get16ByteRandomHexaDecimalNumber() {
		int RANDOM_HEX_LENGTH = 16;
        Random randomService = new Random();
        StringBuilder sb = new StringBuilder();
        while (sb.length() < RANDOM_HEX_LENGTH) {
            sb.append(Integer.toHexString(randomService.nextInt()));
        }
        sb.setLength(RANDOM_HEX_LENGTH);
		return sb.toString();
	}
}