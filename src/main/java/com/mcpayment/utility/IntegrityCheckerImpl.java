package com.mcpayment.utility;

import java.security.MessageDigest;
import java.util.List;
import org.springframework.stereotype.Component;


@Component("IntegrityChecker") 
public class IntegrityCheckerImpl implements IntegrityChecker {
      public String getMD5Hash(List<String> AllParametersExceptHash ) throws Exception {
          StringBuffer hashable = new StringBuffer();
          for (String s : AllParametersExceptHash) {
             hashable.append(s);
          }
          MessageDigest md = MessageDigest.getInstance("MD5");
          byte[] md5 = new byte[64];
          md.update(hashable.toString().getBytes("iso-8859-1"), 0, hashable.length());
          md5 = md.digest();
          return convertedToHex(md5);
      }  

      private String convertedToHex(byte[] data) {
          StringBuffer buf = new StringBuffer();
          for (int i = 0; i < data.length; i++) {
	         int halfOfByte = (data[i] >>> 4) & 0x0F;
	         int twoHalfBytes = 0;
	         do {
	            if ((0 <= halfOfByte) && (halfOfByte <= 9)) {
	                buf.append((char) ('0' + halfOfByte));
	            } else {
	                buf.append((char) ('a' + (halfOfByte - 10)));
	            }
	            halfOfByte = data[i] & 0x0F;
	         } while (twoHalfBytes++ < 1);
          }
          return buf.toString();
      }    
}
